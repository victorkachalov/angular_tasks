import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-zippy',
  templateUrl: './zippy.component.html',
  styleUrls: ['./zippy.component.css']
})
export class ZippyComponent implements OnInit {

  constructor() { }

  @Input('title') title: string;
  isExpanded = true;

  ngOnInit() {
  }

  // метод скрыть/раскрыть для выпадающиего списка
  toggle() {
    this.isExpanded = !this.isExpanded;
  }

}
