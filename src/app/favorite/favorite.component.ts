import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  // переменная по которой будет изменяться класс иконки в html
  isFavorite: boolean;

  // метод для логики toggle звездочки (смена классов)
  onClick() {
    this.isFavorite = !this.isFavorite;
  }

  constructor() { }

  ngOnInit() {
  }



}
