import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent implements OnInit {
  constructor() { }

  // ставим инпуты, чтобы использовать в app-component.html
  @Input('likesCount') likesCount: number;
  @Input('isActive') isActive: boolean;

  // логика для смены класса и подсчета лайков
  onClick() {
    this.likesCount += (this.isActive) ? -1 : 1;
    this.isActive = !this.isActive;
  }

  ngOnInit() {
  }

}
